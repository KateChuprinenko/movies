﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading.Tasks;

namespace MoviesInstaller
{
    [RunInstaller(true)]
    public partial class Installer1 : System.Configuration.Install.Installer
    {
        public Installer1()
        {
            InitializeComponent();
        }

        [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand)]
        public override void Install(IDictionary stateSaver)
        {
            var folder = Context.Parameters["folder"];
            var sid = new SecurityIdentifier(WellKnownSidType.AuthenticatedUserSid, null);

            // Create the rules
            var writeRule = new FileSystemAccessRule(sid, FileSystemRights.Write,
                InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit,
                PropagationFlags.None,
                AccessControlType.Allow);

            if (!string.IsNullOrEmpty(folder) && Directory.Exists(folder))
            {            
                var fsecurity = Directory.GetAccessControl(folder);
                fsecurity.AddAccessRule(writeRule);
                Directory.SetAccessControl(folder, fsecurity);
            }

            base.Install(stateSaver);
        }
    }
}
