﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Movies.Converters
{
    class RatingColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;
            if (int.Parse(parameter.ToString()) == 1)
            {
                if (decimal.Parse(value.ToString()) < (decimal)0.4)
                    return Application.Current.Resources["BadRating"];
                if (decimal.Parse(value.ToString()) > (decimal) 0.8)
                    return Application.Current.Resources["GoodRating"];
                return Application.Current.Resources["NormalRating"];
            }
            if (int.Parse(parameter.ToString()) == 2)
            {
                if (decimal.Parse(value.ToString()) < 4)
                    return Application.Current.Resources["BadRating"];
                if (decimal.Parse(value.ToString()) > 8)
                    return Application.Current.Resources["GoodRating"];
                return Application.Current.Resources["NormalRating"];
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
