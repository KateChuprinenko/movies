﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Movies.Converters
{
    public class VisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return Visibility.Collapsed;
            if (value is IEnumerable<object>)
            {
                if (!((IEnumerable<object>)value).Any() && parameter == null)
                    return Visibility.Collapsed;
                if (((IEnumerable<object>)value).Any() && parameter != null)
                    return Visibility.Collapsed;
            }
            if (value as bool? == false)
                return Visibility.Collapsed;
            if (value.ToString().Length == 0)
                return Visibility.Collapsed;
            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
