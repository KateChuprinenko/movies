﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Movies.Converters
{
    class RoundButtonsImagesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((string) parameter)
            {
                case "Trailer":
                    if (string.IsNullOrWhiteSpace((string)value))
                        return "../Images/NoPlayTrailerIcon.png";
                    return "../Images/PlaytrailerIcon.png";
                case "Watchlist":
                    if ((bool?)value == true)
                        return "../Images/WatchlistIcon.png";
                    return "../Images/NoWatchListIcon.png";
                case "Rating":
                    if ((double)value == 0)
                        return "../Images/NoRateFilmIcon.png";
                    return "../Images/RateFilmIcon.png";
                case "Comment":
                    if (string.IsNullOrWhiteSpace((string) value))
                        return "../Images/NoCommentIcon.png";
                    return "../Images/CommentIcon.png";

            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
