﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Movies.Converters
{
    class FindResultsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility result = Visibility.Collapsed;
            if (value is ObservableCollection<Person>)
                if (((ObservableCollection<Person>) value).Any())
                    result =  Visibility.Visible;
            if (value is ObservableCollection<Movie>)
                if (((ObservableCollection<Movie>)value).Any())
                    result =  Visibility.Visible;
            if (value is ObservableCollection<Show>)
                if (((ObservableCollection<Show>)value).Any())
                    result =  Visibility.Visible;
            if ((string)parameter == "invert")
                return result == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
