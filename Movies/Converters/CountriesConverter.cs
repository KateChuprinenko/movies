﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Movies.Converters
{
    public class CountriesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string result = "";
            if (value is IEnumerable<Country>)
            {
                var collection = ((IEnumerable<Country>)value).ToList();
                if (collection.Any())
                {
                    result = collection.Aggregate(result, (current, item) => current + (item.Name + ", "));
                    result = result.TrimEnd(' ', ',');
                    return result;
                }
            }
            else if (value is IEnumerable<Company>)
            {
                var collection = ((IEnumerable<Company>)value).ToList();
                if (collection.Any())
                {
                    result = collection.Aggregate(result, (current, item) => current + (item.Name + ", "));
                    result = result.TrimEnd(' ', ',');
                    return result;
                }
            }
            else if (value is IEnumerable<string>)
            {
                var collection = ((IEnumerable<string>)value).ToList();
                if (collection.Any())
                {
                    result = string.Join(", ", collection);
                    return result;
                }
            }
            return "-";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}

