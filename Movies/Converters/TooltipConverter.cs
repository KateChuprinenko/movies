﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Movies.Converters
{
    class TooltipConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((string)parameter)
            {
                case "Watchlist":
                    if ((bool?)value == true)
                        return (string)Application.Current.Resources["LocRemoveWatchlistToopTip"];
                    return (string)Application.Current.Resources["LocAddWatchlistToopTip"];
                case "Rating":
                    if ((double)value == 0)
                        return (string)Application.Current.Resources["LocAddRatingToopTip"];
                    return (string)Application.Current.Resources["LocRateToopTip"] + " " + (double)value * 10;
                case "Comment":
                    if (string.IsNullOrWhiteSpace((string)value))
                        return (string)Application.Current.Resources["LocAddCommentToopTip"];
                    return (string)value;

            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
