﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Movies.Converters
{
    public class ImageConverter : IValueConverter
    {
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (string.IsNullOrEmpty((string)value))
                return "../Images/NoImage.jpg";
            const string baseURL = "http://image.tmdb.org/t/p/";
            string fileSize = "w" + parameter;
            return baseURL + fileSize + value;
        }
    }
}
