﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Movies.Converters
{
    public class PersonCreditsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var collection = value as IEnumerable<PersonCredit>;
            var set = new HashSet<string>();
            var result = "";
            if (collection.Any())
            {
                foreach (var item in collection)
                {
                    if (item is PersonCast)
                        set.Add("Actor");
                    else if (item is PersonCrew)
                        set.Add(((PersonCrew)item).Job);
                }

                foreach (var item in set)
                    result += item + ", ";
                result = result.TrimEnd(' ', ',');
                return result;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
