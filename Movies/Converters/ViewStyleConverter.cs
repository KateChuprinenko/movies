﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Movies.Helpers;

namespace Movies.Converters
{
    class ViewStyleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((Enums.View) value == Enums.View.ListView)
                return Application.Current.Resources["ListBoxListStyle"];
            return Application.Current.Resources["ListBoxGridStyle"];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
