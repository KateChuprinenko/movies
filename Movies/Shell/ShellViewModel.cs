﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using System.Windows;
using Caliburn.Micro;
using Movies.Credits;
using Movies.Films;
using Movies.Helpers;
using Movies.Helpers.EventAggregator;
using Movies.Series;
using Movies.OneActor;
using Movies.OneFilm;
using Movies.OneSeries;
using Movies.Search;
using Movies.UserFilms;

namespace Movies.Shell
{
    public sealed class ShellViewModel : Conductor<IScreen>.Collection.OneActive,
        IHandle<ScreenMessage>, IHandle<DetailsMessage>, 
        IHandle<CreditsMessage>, IHandle<bool>
    {
        #region Private Variables

        private readonly IEventAggregator _eventAggregator;

        #endregion

        #region Private Methods

        private void LanguageChanged(object sender, EventArgs e)
        {
            DisplayName = (string)Application.Current.Resources["LocTitle"];
            Properties.Settings.Default.DefaultLanguage = App.Language;
            Properties.Settings.Default.Save();
        }

        #endregion

        #region Public Properties

        private bool _isEnabled;
        public bool IsEnabled
        {
            get { return _isEnabled; }
            set
            {
                _isEnabled = value;
                NotifyOfPropertyChange(() => IsEnabled);
            }
        }

        #endregion

        #region Public Methods

        public void ShowFilms(int category)
        {
            IScreen item = null;
            switch ((Enums.FilmsCategoryEnum)category)
            {
                case Enums.FilmsCategoryEnum.Popular:
                    item = Items.FirstOrDefault(i => i.DisplayName == (string)Application.Current.Resources["LocPopularFilms"]);
                    break;
                case Enums.FilmsCategoryEnum.TopRated:
                    item = Items.FirstOrDefault(i => i.DisplayName == (string)Application.Current.Resources["LocTopFilms"]);
                    break;
                case Enums.FilmsCategoryEnum.NowPlaying:
                    item = Items.FirstOrDefault(i => i.DisplayName == (string)Application.Current.Resources["LocPlayingFilms"]);
                    break;
            }
            if (item == null)
            {
                ActivateItem(IoC.Get<FilmsViewModel>());
                _eventAggregator.PublishOnUIThread(category);
                NotifyOfPropertyChange(() => Items);
            }
            else
                ChangeActiveItem(item, false);
        }

        public void ShowSeries(int category)
        {
            IScreen item = null;
            switch ((Enums.FilmsCategoryEnum)category)
            {
                case Enums.FilmsCategoryEnum.Popular:
                    item = Items.FirstOrDefault(i => i.DisplayName == (string)Application.Current.Resources["LocPopularShows"]);
                    break;
                case Enums.FilmsCategoryEnum.TopRated:
                    item = Items.FirstOrDefault(i => i.DisplayName == (string)Application.Current.Resources["LoctTopShows"]);
                    break;
            }
            if (item == null)
            {
                ActivateItem(IoC.Get<SeriesViewModel>());
                _eventAggregator.PublishOnUIThread(category);
                NotifyOfPropertyChange(() => Items);
            }
            else
                ChangeActiveItem(item, false);
        }

        public void ShowUserFilms(int category)
        {
            IScreen item = null;
            switch ((Enums.UserFilmsCategoryEnum)category)
            {
                case Enums.UserFilmsCategoryEnum.Rated:
                    item = Items.FirstOrDefault(i => i.DisplayName == (string)Application.Current.Resources["LocRatedFilms"]);
                    break;
                case Enums.UserFilmsCategoryEnum.Commented:
                    item = Items.FirstOrDefault(i => i.DisplayName == (string)Application.Current.Resources["LocCommentedFilms"]);
                    break;
                case Enums.UserFilmsCategoryEnum.Watchlist:
                    item = Items.FirstOrDefault(i => i.DisplayName == (string)Application.Current.Resources["LocWatchlist"]);
                    break;
            }
            if (item == null)
            {
                ActivateItem(IoC.Get<UserFilmsViewModel>());
                _eventAggregator.PublishOnUIThread(category);
                NotifyOfPropertyChange(() => Items);
            }
            else
                ChangeActiveItem(item, false);
        }

        public void Find()
        {
            var item = Items.FirstOrDefault(i => i.DisplayName == (string)Application.Current.Resources["LocSearch"]);
            if (item == null)
            {
                ActivateItem(IoC.Get<SearchViewModel>());
                NotifyOfPropertyChange(() => Items);
            }
            else
                ChangeActiveItem(item, false);
        }

        public void CloseItem(IScreen screen)
        {
            DeactivateItem(screen, true);
            NotifyOfPropertyChange(() => Items);
        }

        public void OpenLink(string uri)
        {
            try
            {
                Process.Start(new ProcessStartInfo(uri));
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "OpenLink", ex,
                    "link: " + uri));
            }
        }

        public void ChangeLanguage(int category)
        {
            CultureInfo culture = null;
            switch ((Enums.Language) category)
            {
                case Enums.Language.English:
                    culture = new CultureInfo("en-US");
                    break;
                case Enums.Language.Russian:
                    culture = new CultureInfo("ru-RU");
                    break;
            }
            if (!Equals(CultureInfo.CurrentUICulture, culture))
            {
                if (Items.Count > 3)
                    MessageBox.Show((string) Application.Current.Resources["LocChangeLanguageMessage"]);
                else
                    App.Language = culture;
            }
        }

        #endregion

        #region IHandle Implementation

        public void Handle(bool isEnabled)
        {
            IsEnabled = isEnabled;
        }

        public void Handle(ScreenMessage screen)
        {
            if (screen.CloseScreen)
                CloseItem(screen.Screen);
            else
                ChangeActiveItem(screen.Screen, false);
            NotifyOfPropertyChange(() => Items);
        }

        public void Handle(DetailsMessage item)
        {
            if (item.DetailedItem is Movie)
            {
                ActivateItem(IoC.Get<OneFilmViewModel>());
                _eventAggregator.PublishOnUIThread(item.DetailedItem);
            }
            if (item.DetailedItem is Show)
            {
                ActivateItem(IoC.Get<OneSeriesViewModel>());
                _eventAggregator.PublishOnUIThread(item.DetailedItem);
            }
            if (item.DetailedItem is Person)
            {
                ActivateItem(IoC.Get<OneActorViewModel>());
                _eventAggregator.PublishOnUIThread(item.DetailedItem);
            }
            NotifyOfPropertyChange(() => Items);
        }

        public void Handle(CreditsMessage item)
        {
            ActivateItem(IoC.Get<CreditsViewModel>());
            _eventAggregator.PublishOnUIThread(item.Item);
            NotifyOfPropertyChange(() => Items);
        }

        #endregion

        #region Constructor

        public ShellViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _eventAggregator.Subscribe(this);

            IsEnabled = true;
            App.LanguageChanged += LanguageChanged;
            App.Language = Properties.Settings.Default.DefaultLanguage;
            DisplayName = (string)Application.Current.Resources["LocTitle"];
        }
        #endregion
    }
}
