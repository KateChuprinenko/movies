﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Caliburn.Micro;
using System.Net.TMDb;
using System.Windows;
using Services;
using Movies.Films;
using Movies.Series;
using Movies.Helpers.EventAggregator;
using Movies.Actors;
using Movies.Helpers;
using Movies.NoFindResults;
using Newtonsoft.Json;
using Services.Search;

namespace Movies.Search
{
    public sealed class SearchViewModel : Conductor<object>, IHandle<NoResults>
    {
        public class ItemsCategory
        {
            public int Id { get; set; }
            public string Name { get; set; }

            public ItemsCategory(int id, string name)
            {
                Id = id;
                Name = name;
            }
        }

        #region Private Variables

        private readonly IEventAggregator _eventAggregator;
        private readonly ISearchProvider _provider;
        private List<int> _genres;

        #endregion

        #region Public Properties

        private string _title;

        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                NotifyOfPropertyChange(() => Title);
                NotifyOfPropertyChange(() => CanFind);
            }
        }

        private ItemsCategory _selectedFilterCategory;
        public ItemsCategory SelectedFilterCategory
        {
            get { return _selectedFilterCategory; }
            set
            {
                _selectedFilterCategory = value;
                NotifyOfPropertyChange(() => SelectedFilterCategory);
            }
        }

        private ItemsCategory _selectedCategory;
        public ItemsCategory SelectedCategory
        {
            get { return _selectedCategory; }
            set
            {
                _selectedCategory = value;
                NotifyOfPropertyChange(() => SelectedCategory);
            }
        }

        public ObservableCollection<Genre> SelectedGenres { get; set; }
        public ObservableCollection<string> Years { get; set; }
        public ObservableCollection<Genre> Genres { get; }
        public ObservableCollection<ItemsCategory> Categories { get; set; }
        public ObservableCollection<ItemsCategory> FilterCategories { get; set; }
        public string MinDate { get; set; }
        public string MaxDate { get; set; }
        public string Creator { get; set; }
        public string Actor { get; set; }
        public decimal? Rating { get; set; }
        #endregion

        #region Private Methods

        private async void GetGenres()
        {
            try
            {
                var genres = await _provider.GetGenres(CultureInfo.CurrentUICulture);
                Genres.Clear();
                foreach (var item in genres)
                    Genres.Add(item);
                if (_genres != null &&_genres.Any())
                {
                    SelectedGenres.Clear();
                    foreach (var item in _genres)
                        SelectedGenres.Add(Genres.FirstOrDefault(i => i.Id == item));
                }
                NotifyOfPropertyChange(() => Genres);
            }
            catch (AggregateException ex)
            {
                Logging.HandleNetworkError(new LogString(GetType().Name, "GetJenres", ex));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (Exception ex)
            {
                Logging.HandleRequestError(new LogString(GetType().Name, "GetJenres", ex));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
        }

        private void GetDates(out DateTime? min, out DateTime? max)
        {
            if (MinDate != "-" && MaxDate != "-")
            {
                if (int.Parse(MaxDate) < int.Parse(MinDate))
                {
                    var temp = MinDate;
                    MinDate = MaxDate;
                    MaxDate = temp;
                }
                NotifyOfPropertyChange(() => MinDate);
                NotifyOfPropertyChange(() => MaxDate);
            }

            if (MinDate == "-")
                min = null;
            else
                min = DateTime.Parse("01.01." + MinDate);

            if (MaxDate == "-")
                max = null;
            else
                max = DateTime.Parse("31.12." + MaxDate);
        }

        private string GetGenresString()
        {
            if (SelectedGenres.Any())
            {
                var genresId = SelectedGenres.Select(g => g.Id.ToString());
                return string.Join(",", genresId);
            }
            return null;
        }

        private void AppOnLanguageChanged(object sender, EventArgs eventArgs)
        {
            DisplayName = (string)Application.Current.Resources["LocSearch"];

            RefreshCategories();
            _genres = SelectedGenres.Select(i => i.Id).ToList();
            GetGenres();
        }

        private void RefreshCategories()
        {
            var categoryId = SelectedCategory.Id;
            var filterCategoryId = SelectedFilterCategory.Id;

            SelectedCategory = null;
            SelectedFilterCategory = null;

            Categories[0].Name = (string)Application.Current.Resources["LocMovies"];
            Categories[1].Name = (string)Application.Current.Resources["LocShows"];
            Categories[2].Name = (string)Application.Current.Resources["LocCelebs"];

            FilterCategories[0].Name = (string) Application.Current.Resources["LocMovies"];
            FilterCategories[1].Name = (string)Application.Current.Resources["LocShows"];

            SelectedCategory = Categories.FirstOrDefault(i => i.Id == categoryId);
            SelectedFilterCategory = FilterCategories.FirstOrDefault(i => i.Id == filterCategoryId);

            NotifyOfPropertyChange(() => Categories);
            NotifyOfPropertyChange(() => FilterCategories);
        }

        #endregion

        #region Public Methods

        public async void Filter()
        {
            DateTime? max;
            DateTime? min;
            GetDates(out min, out max);
            string castString = null;
            string crewString = null;

            if (SelectedFilterCategory.Id == 1)
            {
                try
                {
                    if (!string.IsNullOrWhiteSpace(Actor))
                    {
                        castString = await _provider.GetPeopleString(Actor, CultureInfo.CurrentUICulture);
                        if (string.IsNullOrWhiteSpace(castString))
                        {
                            Xceed.Wpf.Toolkit.MessageBox.Show(
                                (string) Application.Current.Resources["LocNoActorMessage"]);
                            Actor = null;
                            NotifyOfPropertyChange(() => Actor);
                        }
                    }
                }
                catch (AggregateException ex)
                {
                    Logging.HandleNetworkError(new LogString(GetType().Name, "Filter", ex,
                        "actor: " + Actor));
                    _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
                }
                catch (ServiceRequestException ex)
                {
                    Logging.HandleRequestError(new LogString(GetType().Name, "Filter", ex,
                        "actor: " + Actor));
                    _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
                }
                catch (Exception ex)
                {
                    Logging.HandleUnknownError(new LogString(GetType().Name, "Filter", ex,
                        "actor: " + Actor));
                    _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
                }

                try
                {
                    if (!string.IsNullOrWhiteSpace(Creator))
                    {
                        crewString = await _provider.GetPeopleString(Creator, CultureInfo.CurrentUICulture);
                        if (string.IsNullOrWhiteSpace(crewString))
                        {
                            Xceed.Wpf.Toolkit.MessageBox.Show(
                                (string) Application.Current.Resources["LocNoCreatorMessage"]);
                            Creator = null;
                            NotifyOfPropertyChange(() => Creator);
                        }
                    }
                }
                catch (ServiceRequestException ex)
                {
                    Logging.HandleRequestError(new LogString(GetType().Name, "Filter", ex,
                        "creator: " + Creator));
                    _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
                }
                catch (AggregateException ex)
                {
                    Logging.HandleNetworkError(new LogString(GetType().Name, "Filter", ex,
                        "creator: " + Creator));
                    _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
                }
                catch (Exception ex)
                {
                    Logging.HandleUnknownError(new LogString(GetType().Name, "Filter", ex,
                        "creator: " + Creator));
                    _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
                }

                ActivateItem(IoC.Get<FilmsViewModel>());
                _eventAggregator.PublishOnUIThread(new FilmQueryMessage(min, max, GetGenresString(), Rating, castString, crewString));
            }
            else
            {
                ActivateItem(IoC.Get<SeriesViewModel>());
                _eventAggregator.PublishOnUIThread(new FilmQueryMessage(min, max, GetGenresString(), Rating, null, null));
            }

        }
        public void Find()
        {
            if (SelectedCategory.Id == 1)
                ActivateItem(IoC.Get<FilmsViewModel>());
            else if (SelectedCategory.Id == 2)
                ActivateItem(IoC.Get<SeriesViewModel>());
            else
                ActivateItem(IoC.Get<ActorsViewModel>());

            _eventAggregator.PublishOnUIThread(Title);
        }
        public bool CanFind
        {
            get { return !string.IsNullOrWhiteSpace(Title); }
        }

        #endregion

        #region IHandle Implementation

        public void Handle(NoResults message)
        {
            ActivateItem(IoC.Get<NoFindResultsViewModel>());
        }

        #endregion

        #region Constructor

        public SearchViewModel(ISearchProvider provider, IEventAggregator eventAggregator)
        {
            _provider = provider;
            _eventAggregator = eventAggregator;
            _eventAggregator.Subscribe(this);

            DisplayName = (string)Application.Current.Resources["LocSearch"];

            App.LanguageChanged += AppOnLanguageChanged;

            Years = new ObservableCollection<string> { "-" };
            var years = Enumerable.Range(1890, 128).Select(y => y.ToString()).ToList().OrderByDescending(y => y);
            foreach (var item in years)
                Years.Add(item);

            Rating = 5;

            MinDate = "-";
            MaxDate = "-";

            Genres = new ObservableCollection<Genre>();
            SelectedGenres = new ObservableCollection<Genre>();
            Categories = new ObservableCollection<ItemsCategory>
            {
                new ItemsCategory(1, (string) Application.Current.Resources["LocMovies"]),
                new ItemsCategory(2, (string) Application.Current.Resources["LocShows"]),
                new ItemsCategory(3, (string) Application.Current.Resources["LocCelebs"])
            };

            FilterCategories = new ObservableCollection<ItemsCategory>
            {
                new ItemsCategory(1, (string) Application.Current.Resources["LocMovies"]),
                new ItemsCategory(2, (string) Application.Current.Resources["LocShows"])
            };

            SelectedCategory = Categories.FirstOrDefault(i => i.Id == 1);
            SelectedFilterCategory = FilterCategories.FirstOrDefault(i => i.Id == 1);

            GetGenres();
        }

        #endregion
    }
}
