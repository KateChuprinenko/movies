﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using Movies.Helpers;
using Movies.Helpers.EventAggregator;
using Newtonsoft.Json;
using Services;
using Services.Series;

namespace Movies.Series
{
    sealed class SeriesViewModel : Screen, IHandle<int>, IHandle<string>, IHandle<FilmQueryMessage>
    {
        #region Private Variables

        private readonly ISeriesProvider _provider;
        private readonly IEventAggregator _eventAggregator;
        private readonly IEventAggregator _localAggregator;
        private Func<int, CultureInfo, Task<Shows>> GetData;
        private string _title;
        private FilmQueryMessage _filmQuery;
        private Enums.FilmsCategoryEnum _currentCategory;

        #endregion

        #region Public Properties

        private Enums.View _viewProperty;
        public Enums.View ViewProperty
        {
            get { return _viewProperty; }
            set
            {
                if (_viewProperty != value)
                {
                    _viewProperty = value;
                    NotifyOfPropertyChange(() => ViewProperty);
                }
            }
        }

        private Show _selectedSeries;
        public Show SelectedSeries
        {
            get { return _selectedSeries; }
            set
            {
                if (_selectedSeries != value)
                {
                    _eventAggregator.PublishOnUIThread(new DetailsMessage(value));
                    _selectedSeries = null;
                    NotifyOfPropertyChange(() => SelectedSeries);
                }
            }
        }

        private int _currentPage;
        public int CurrentPage
        {
            get { return _currentPage; }
            set
            {
                _currentPage = value;
                NotifyOfPropertyChange(() => CurrentPage);
                NotifyOfPropertyChange(() => CanMoveNext);
                NotifyOfPropertyChange(() => CanMovePrev);
            }
        }

        private int _maxPage;
        public int MaxPage
        {
            set
            {
                _maxPage = value > 1000 ? 1000 : value;
                NotifyOfPropertyChange(() => CanMoveNext);
            }
            get { return _maxPage; }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        public ObservableCollection<Show> Series { get; }

        #endregion

        #region Private Methods

        private void AppOnLanguageChanged(object sender, EventArgs eventArgs)
        {
            IsBusy = true;
            if (_currentCategory != 0)
            {
                _localAggregator.PublishOnUIThread((int)_currentCategory);
                return;
            }
            RefreshItems();
        }

        private async void RefreshItems()
        {
            try
            {
                if (GetData != null)
                {
                    IsBusy = false;
                    Series.Clear();
                    var series = await GetData(CurrentPage, CultureInfo.CurrentUICulture);
                    MaxPage = series.PageCount;
                    foreach (var show in series.Results)
                        Series.Add(show);
                    NotifyOfPropertyChange(() => Series);
                    return;
                }
            }
            catch (AggregateException ex)
            {
                Logging.HandleNetworkError(new LogString(GetType().Name, "RefreshItems", ex));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (ServiceRequestException ex)
            {
                Logging.HandleRequestError(new LogString(GetType().Name, "RefreshItems", ex));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "RefreshItems", ex));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }

            try
            {
                if (!string.IsNullOrEmpty(_title))
                {
                    Series.Clear();
                    var series = await _provider.FindShowsByTitle(_title, CurrentPage, CultureInfo.CurrentUICulture);
                    if (!series.Results.Any())
                    {
                        IsBusy = false;
                        _eventAggregator.PublishOnUIThread(new NoResults());
                        return;
                    }
                    MaxPage = series.PageCount;
                    foreach (var show in series.Results)
                        Series.Add(show);
                    IsBusy = false;
                    NotifyOfPropertyChange(() => Series);
                    return;
                }
            }
            catch (AggregateException ex)
            {
                Logging.HandleNetworkError(new LogString(GetType().Name, "RefreshItems", ex, "title: " + _title));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (ServiceRequestException ex)
            {
                Logging.HandleRequestError(new LogString(GetType().Name, "RefreshItems", ex, "title: " + _title));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "RefreshItems", ex, "title: " + _title));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }

            try
            {
                if (_filmQuery != null)
                {
                    Series.Clear();
                    var series = await _provider.DiscoverShows(_filmQuery.MinDate, _filmQuery.MaxDate,
                        _filmQuery.MinRating, _filmQuery.Genres, 
                        CurrentPage, CultureInfo.CurrentUICulture);
                    if (!series.Results.Any())
                    {
                        IsBusy = false;
                        _eventAggregator.PublishOnUIThread(new NoResults());
                        return;
                    }
                    MaxPage = series.PageCount;
                    foreach (var show in series.Results)
                        Series.Add(show);
                    IsBusy = false;
                    NotifyOfPropertyChange(() => Series);
                }
            }
            catch (ServiceRequestException ex)
            {
                Logging.HandleRequestError(new LogString(GetType().Name, "RefreshItems", ex,
                    "filmQuery: " + JsonConvert.SerializeObject(_filmQuery)));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (AggregateException ex)
            {
                Logging.HandleNetworkError(new LogString(GetType().Name, "RefreshItems", ex,
                    "filmQuery: " + JsonConvert.SerializeObject(_filmQuery)));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "RefreshItems", ex,
                    "filmQuery: " + JsonConvert.SerializeObject(_filmQuery)));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
        }

        #endregion

        #region Ihandle Implementation

        public void Handle(int category)
        {
            try
            {
                _eventAggregator.Unsubscribe(this);
                _currentCategory = (Enums.FilmsCategoryEnum) category;
                switch (_currentCategory)
                {
                    case Enums.FilmsCategoryEnum.Popular:
                        DisplayName = (string)Application.Current.Resources["LocPopularShows"];
                        GetData = _provider.GetPopularShows;
                        break;
                    case Enums.FilmsCategoryEnum.TopRated:
                        DisplayName = (string)Application.Current.Resources["LocTopShows"];
                        GetData = _provider.GetTopRatedShows;
                        break;
                }
                RefreshItems();
            }
            catch (AggregateException ex)
            {
                Logging.HandleNetworkError(new LogString(GetType().Name, "Handle(int)", ex,
                        "category: " + (Enums.FilmsCategoryEnum)category));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (ServiceRequestException ex)
            {
                Logging.HandleRequestError(new LogString(GetType().Name, "Handle(int)", ex,
                        "category: " + (Enums.FilmsCategoryEnum)category));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "Handle(int)", ex,
                    "category: " + (Enums.FilmsCategoryEnum) category));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }

        }

        public void Handle(string query)
        {
            _eventAggregator.Unsubscribe(this);
            _title = query;
            RefreshItems();
        }

        public void Handle(FilmQueryMessage message)
        {
            _eventAggregator.Unsubscribe(this);
            _filmQuery = message;
            RefreshItems();
        }

        #endregion 

        #region Paging Methods

        public bool CanMovePrev
        {
            get { return _currentPage > 1; }
        }

        public bool CanMoveNext
        {
            get { return _currentPage < MaxPage; }
        }

        public void MoveFirst()
        {
            if (CurrentPage == 1)
                return;
            CurrentPage = 1;
            RefreshItems();
        }

        public void MovePrev()
        {
            CurrentPage -= 1;
            RefreshItems();
        }

        public void MoveNext()
        {
            CurrentPage += 1;
            RefreshItems();
        }

        public void MoveLast()
        {
            if (CurrentPage == MaxPage)
                return;
            CurrentPage = MaxPage;
            RefreshItems();
        }

        public void ChangeView(int view)
        {
            ViewProperty = (Enums.View)view;
        }

        #endregion

        #region Constructor

        public SeriesViewModel(ISeriesProvider provider, IEventAggregator eventAggregator)
        {
            _provider = provider;
            _eventAggregator = eventAggregator;
            _localAggregator = new EventAggregator();
            _eventAggregator.Subscribe(this);
            _localAggregator.Subscribe(this);

            DisplayName = "";

            App.LanguageChanged += AppOnLanguageChanged;

            ViewProperty = Enums.View.GridView;
            IsBusy = true;
            Series = new ObservableCollection<Show>();

            CurrentPage = 1;
        }
        #endregion
    }
}
