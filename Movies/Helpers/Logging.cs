﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using NLog;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace Movies.Helpers
{
    public class LogString
    {
        public string Method { get; set; }
        public string ClassName { get; set; }
        public Exception Exception { get; set; }
        public string ExtraInfo { get; set; }

        public LogString(string className, string method, Exception exception, string extra = null)
        {
            ClassName = className;
            Method = method;
            Exception = exception;
            ExtraInfo = extra;
        }
    }

    public static class Logging
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static void LogError(LogString logString)
        {
            Logger.Error("Class: {0}.\nMethod: {1}.\nExtra: {2}.\nException Message: {3}.\n" +
                            "Inner Exception: {4}.\nSource: {5}.", logString.ClassName, logString.Method,
                            logString.ExtraInfo, logString.Exception.Message,
                            logString.Exception.InnerException, logString.Exception.Source);
        }

        public static void HandleRequestError(LogString logString)
        {
            LogError(logString);
            MessageBox.Show((string) Application.Current.Resources["LocRequestException"]);
        }

        public static void HandleNetworkError(LogString logString)
        {
            LogError(logString);
            MessageBox.Show((string)Application.Current.Resources["LocNetworkException"]);
        }

        public static void HandleUnknownError(LogString logString)
        {
            LogError(logString);
            MessageBox.Show((string)Application.Current.Resources["LocUnknownException"]);
        }

        public static void HandleFatalError(LogString logString)
        {
            LogError(logString);
            MessageBox.Show((string)Application.Current.Resources["LocFatalException"]);
        }
    }
}

