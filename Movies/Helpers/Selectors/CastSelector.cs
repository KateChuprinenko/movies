﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Movies.Helpers.Selectors
{
    class CastSelector : DataTemplateSelector
    {
        public DataTemplate PersonCast { get; set; }
        public DataTemplate FilmCast { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is PersonCast)
                return PersonCast;
            if (item is MediaCast)
                return FilmCast;
            return null;
        }
    }
}
