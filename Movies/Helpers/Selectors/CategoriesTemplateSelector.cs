﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Movies.Helpers.Selectors
{
    class CategoriesTemplateSelector : DataTemplateSelector
    {
        public DataTemplate PersonTemplate { get; set; }
        public DataTemplate FilmTemplate { get; set; }
        public DataTemplate ShowTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is Person)
                return PersonTemplate;
            if (item is Show)
                return ShowTemplate;
            if (item is Movie)
                return FilmTemplate;
            return null;
        }
    }
}
