﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Movies.Helpers.Selectors
{
    public class ActorCreditTemplateSelector : DataTemplateSelector
    {
        public DataTemplate CastTemplate { get; set; }
        public DataTemplate CrewTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is PersonCast)
                return CastTemplate;
            if (item is PersonCrew)
                return CrewTemplate;
            return null;
        }
    }
}
