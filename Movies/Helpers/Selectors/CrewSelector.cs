﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Movies.Helpers.Selectors
{
    class CrewSelector : DataTemplateSelector
    {
        public DataTemplate PersonCrew { get; set; }
        public DataTemplate FilmCrew { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is PersonCrew)
                return PersonCrew;
            if (item is MediaCrew)
                return FilmCrew;
            return null;
        }
    }
}
