﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace Movies.Helpers.EventAggregator
{
    public class ScreenMessage
    {
        public Screen Screen { get; }
        public bool CloseScreen { get; }

        public ScreenMessage(Screen screen, bool closeScreen)
        {
            Screen = screen;
            CloseScreen = closeScreen;
        }
    }
}
