﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Helpers.EventAggregator
{
    public class CreditsMessage
    {
        public object Item { get; set; }

        public CreditsMessage(object item)
        {
            Item = item;
        }
    }
}
