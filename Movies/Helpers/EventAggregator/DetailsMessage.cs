﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Helpers.EventAggregator
{
    public class DetailsMessage
    {
        public object DetailedItem;

        public DetailsMessage(object item)
        {
            DetailedItem = item;
        }
    }
}
