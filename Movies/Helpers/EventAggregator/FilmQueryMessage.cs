﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Helpers.EventAggregator
{
    public class FilmQueryMessage
    {
        public FilmQueryMessage(DateTime? minDate, DateTime? maxDate, string genres, decimal? rating, string actor, string creator)
        {
            MinDate = minDate;
            MaxDate = maxDate;
            Genres = genres;
            MinRating = rating;
            Actor = actor;
            Creator = creator;
        }

        public DateTime? MinDate { get; set; }
        public DateTime? MaxDate { get; set; }
        public decimal? MinRating { get; set; }
        public string Actor { get; set; }
        public string Creator { get; set; }
        public string Genres { get; set; }
    }
}
