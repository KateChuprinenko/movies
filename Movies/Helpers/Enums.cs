﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Helpers
{
    class Enums
    {
        public enum FilmsCategoryEnum
        {
            Popular=1,
            TopRated,
            NowPlaying
        }

        public enum UserFilmsCategoryEnum
        {
            Rated = 1,
            Commented,
            Watchlist
        }

        public enum DatabaseCategoryEnum
        {
            Movie=1,
            Show
        }

        public enum Language
        {
            English = 1,
            Russian
        }

        public enum View
        {
            GridView = 1,
            ListView
        }
    }
}
