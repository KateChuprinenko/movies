﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Threading;
using Caliburn.Micro;
using Movies.Actors;
using Movies.Credits;
using Movies.Films;
using Movies.Helpers;
using Movies.NoFindResults;
using Movies.OneActor;
using Movies.OneFilm;
using Movies.OneSeries;
using Movies.Search;
using Movies.Series;
using Movies.Shell;
using Movies.UserFilms;
using Movies.Views;
using Services;
using Services.Actors;
using Services.Films;
using Services.Search;
using Services.Series;
using Services.UserDB;

namespace Movies
{
    public class AppBootstrapper : BootstrapperBase
    {

        #region Private Variables

        private readonly SimpleContainer _myContainer = new SimpleContainer();

        #endregion

        #region Override Methods

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<ShellViewModel>();
        }

        protected override object GetInstance(Type serviceType, string key)
        {
            var instance = _myContainer.GetInstance(serviceType, key);
            return instance;
        }

        protected override IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return _myContainer.GetAllInstances(serviceType);
        }

        protected override void BuildUp(object instance)
        {
            _myContainer.BuildUp(instance);
        }

        protected override void Configure()
        {
            _myContainer.Singleton<IActorProvider, ActorProvider>();
            _myContainer.Singleton<IFilmsProvider, FilmsProvider>();
            _myContainer.Singleton<ISearchProvider, SearchProvider>();
            _myContainer.Singleton<ISeriesProvider, SeriesProvider>();
            _myContainer.Singleton<IUserDbProvider, UserDbProvider>();
            _myContainer.Singleton<IEventAggregator, EventAggregator>();
            _myContainer.Singleton<IWindowManager, WindowManager>();

            _myContainer.Singleton<ShellViewModel>();
            _myContainer.Singleton<NoFindResultsViewModel>();

            _myContainer.PerRequest<TrailerWindow>();
            _myContainer.PerRequest<SearchViewModel>();
            _myContainer.PerRequest<FilmsViewModel>();
            _myContainer.PerRequest<SeriesViewModel>();
            _myContainer.PerRequest<ActorsViewModel>();
            _myContainer.PerRequest<UserFilmsViewModel>();
            _myContainer.PerRequest<CreditsViewModel>();
            _myContainer.PerRequest<OneActorViewModel>();
            _myContainer.PerRequest<OneFilmViewModel>();
            _myContainer.PerRequest<OneSeriesViewModel>();
        }

        protected override void OnUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            Logging.HandleFatalError(new LogString(GetType().Name, "", e.Exception, "Fatal Error!"));

            e.Handled = true;
            Application.Current.Shutdown();
        }

        #endregion

        #region Constructor

        public AppBootstrapper()
        {
            Initialize();
        }

        #endregion
    }
}
