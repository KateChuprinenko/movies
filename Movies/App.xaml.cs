﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;


namespace Movies
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static event EventHandler LanguageChanged;
        public static CultureInfo Language
        {
            get { return CultureInfo.CurrentUICulture; }
            set
            {
                CultureInfo.CurrentUICulture = value;

                var dictionary = new ResourceDictionary
                {
                    Source = new Uri($"Resources/Language.{value.Name}.xaml", UriKind.Relative)
                };
                Current.Resources.MergedDictionaries.Remove(Current.Resources.MergedDictionaries[0]);
                Current.Resources.MergedDictionaries.Insert(0, dictionary);
                LanguageChanged?.Invoke(Current, new EventArgs());
            }
        }
    }
}
