﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using System.Windows;
using System.Windows.Controls.Primitives;
using Caliburn.Micro;
using CefSharp;
using CefSharp.Wpf;
using Movies.Helpers;
using Movies.Helpers.EventAggregator;
using Movies.Views;
using Newtonsoft.Json;
using Services;
using Services.Actors;
using Services.Films;
using Services.UserDB;

namespace Movies.OneFilm
{
    public sealed class OneFilmViewModel : Screen, IHandle<Movie>
    {
        #region Private Variables

        private readonly IFilmsProvider _filmsProvider;
        private readonly IActorProvider _actorProvider;
        private readonly IUserDbProvider _userDbProvider;
        private readonly IEventAggregator _eventAggregator;
        private readonly IEventAggregator _localAggregator;
        private bool _error;
        private int _filmId;
        private Movie _currentMovie;

        #endregion

        #region Public Properties

        private Movie _film;
        public Movie Film
        {
            get { return _film; }
            set
            {
                SetFilm(value);
            }
        }

        private bool _watchlist;
        public bool Watchlist
        {
            get { return _watchlist; }
            set
            {
                _watchlist = value;
                NotifyOfPropertyChange(() => Watchlist);
            }
        }

        private string _currentComment;
        public string CurrentComment
        {
            get { return _currentComment; }
            set
            {
                _currentComment = string.IsNullOrWhiteSpace(value) ? null : value;
                NotifyOfPropertyChange(() => CurrentComment);
            }
        }

        private string _comment;
        public string Comment
        {
            get { return _comment; }
            set
            {
                _comment = string.IsNullOrWhiteSpace(value) ? null : value;
                NotifyOfPropertyChange(() => Comment);
            }
        }

        private int _userRating;
        public double UserRating
        {
            get { return (double)_userRating / 10; }
            set
            {
                try
                {
                    _userRating = (int)(value * 10);
                    Rate = false;
                    if (_userRating != 0)
                        _userDbProvider.SaveRating(_filmId, _userRating, (int)Enums.DatabaseCategoryEnum.Movie);
                    NotifyOfPropertyChange(() => UserRating);
                }
                catch (Exception ex)
                {
                    Logging.HandleUnknownError(new LogString(GetType().Name, "set UserRating", ex));
                    _error = true;
                    _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
                }
            }
        }

        private string _trailer;
        public string Trailer
        {
            get { return _trailer; }
            set
            {
                _trailer = value;
                NotifyOfPropertyChange(() => Trailer);
                NotifyOfPropertyChange(() => CanWatchTrailer);
            }
        }

        private bool _showComment;
        public bool ShowComment
        {
            get { return _showComment; }
            set
            {
                _showComment = value;
                NotifyOfPropertyChange(() => ShowComment);
            }
        }

        private bool _rate;
        public bool Rate
        {
            get { return _rate; }
            set
            {
                _rate = value;
                NotifyOfPropertyChange(() => Rate);
            }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        public bool CanWatchTrailer
        {
            get { return !string.IsNullOrWhiteSpace(Trailer); }
        }

        private MediaCast _selectedActor;
        public MediaCast SelectedActor
        {
            get { return _selectedActor; }
            set
            {
                if (_selectedActor != value)
                {
                    _selectedActor = null;
                    NotifyOfPropertyChange(() => SelectedActor);
                    PublishActor(value);
                }
            }
        }

        public ObservableCollection<MediaCast> FilmCast { get; }
        public double Rating { get; set; }

        #endregion

        #region Private Methods

        private void AppOnLanguageChanged(object sender, EventArgs eventArgs)
        {
            _localAggregator.PublishOnUIThreadAsync(_currentMovie);
            IsBusy = true;
        }

        private async void PublishActor(MediaCast actor)
        {
            try
            {
                var man = await _actorProvider.GetPersonById(actor.Id, CultureInfo.CurrentUICulture);
                _eventAggregator.PublishOnUIThread(new DetailsMessage(man));
            }
            catch (AggregateException ex)
            {
                Logging.HandleNetworkError(new LogString(GetType().Name, "Handle(MediaCast)", ex,
                        "mediacast id: " + actor.Id));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (ServiceRequestException ex)
            {
                Logging.HandleRequestError(new LogString(GetType().Name, "Handle(MediaCast)", ex,
                        "mediacast id: " + actor.Id));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "Handle(MediaCast)", ex,
                    "mediacast id: " + actor.Id));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
        }

        private async void SetFilm(Movie film)
        {
            try
            {
                _film = await _filmsProvider.GetFilmById(film.Id, CultureInfo.CurrentUICulture);
                _film.Images = await _filmsProvider.GetMoviesImages(film.Id);

                DisplayName = _film.Title;

                FilmCast.Clear();
                foreach (var man in _film.Credits.Cast.Take(7))
                {
                    if (!string.IsNullOrWhiteSpace(man.Name))
                    {
                        var actor = await _actorProvider.GetPersonById(man.Id, CultureInfo.CurrentUICulture);
                        man.Image = actor.Poster;
                        FilmCast.Add(man);
                    }
                }
                Trailer = _film.Videos.Results.FirstOrDefault(v => v.Type == "Trailer")?.Key;
                NotifyOfPropertyChange(() => Film);
                NotifyOfPropertyChange(() => FilmCast);
                IsBusy = false;
            }
            catch (AggregateException ex)
            {
                Logging.HandleNetworkError(new LogString(GetType().Name, "SetFilm(Movie)", ex,
                        "movie id: " + film.Id));
                _error = true;
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (ServiceRequestException ex)
            {
                Logging.HandleRequestError(new LogString(GetType().Name, "SetFilm(Movie)", ex,
                        "movie id: " + film.Id));
                _error = true;
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "SetFilm(Movie)", ex,
                    "movie id: " + film.Id));
                _error = true;
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
        }

        private void SetUserMovie(int id)
        {
            try
            {
                var userMovie = _userDbProvider.GetUserMovie(id, (int)Enums.DatabaseCategoryEnum.Movie);
                UserRating = (double)userMovie.Rating / 10;
                Comment = userMovie.Comment;
                Watchlist = userMovie.Watchlist;

                _currentComment = Comment;
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "SetUserMovie", ex,
                    "movie id: " + id));
                _error = true;
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
        }

        #endregion

        #region Public Methods

        public void ChangeWatchlist()
        {
            try
            {
                Watchlist = !Watchlist;
                _userDbProvider.SaveWatchlist(Film.Id, Watchlist, (int)Enums.DatabaseCategoryEnum.Movie);
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "ChangeWatchlist", ex));
                _error = true;
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
        }

        public void WatchTrailer()
        {
            try
            {
                _eventAggregator.PublishOnUIThread(false);
                var browser = new ChromiumWebBrowser
                {
                    Address = string.Concat("https://www.youtube.com/embed/", Trailer),
                    LifeSpanHandler = new UserLifeSpanHandler()
                };
                var window = IoC.Get<TrailerWindow>();
                window.Content = browser;
                window.Title = Film.Title;
                window.ShowDialog();
                _eventAggregator.PublishOnUIThread(true);
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "WatchTrailer", ex));
                _error = true;
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
        }

        public void RateFilm()
        {
            Rate = !Rate;
        }

        public void WatchComment()
        {
            ShowComment = true;
        }

        public void SaveReview()
        {
            try
            {
                Comment = CurrentComment;
                ShowComment = false;
                _userDbProvider.SaveComment(Film.Id, Comment, (int)Enums.DatabaseCategoryEnum.Movie);
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "SaveReview", ex));
                _error = true;
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
        }

        public void CloseReview()
        {
            ShowComment = false;
        }

        public void ShowAll()
        {
            _eventAggregator.PublishOnUIThread(new CreditsMessage(Film));
        }

        #endregion

        #region Override Methods

        public override void CanClose(Action<bool> callback)
        {
            if (CurrentComment != Comment && !_error)
            {
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, false));
                ShowComment = true;
                var result = MessageBox.Show((string)Application.Current.Resources["LocCloseReview"], Film.Title, MessageBoxButton.YesNo);
                callback(result == MessageBoxResult.Yes);
                return;
            }
            callback(true);
        }

        #endregion

        #region IHandle Implementation

        public void Handle(Movie movie)
        {
            _eventAggregator.Unsubscribe(this);

            _currentMovie = movie;
            _filmId = movie.Id;
            Film = movie;
            SetUserMovie(_filmId);
            Rating = (double)(movie.VoteAverage / 10);
        }

        #endregion

        #region Constructor

        public OneFilmViewModel(IFilmsProvider filmProvider, IActorProvider actorProvider,
            IUserDbProvider userDbProvider, IEventAggregator eventAggregator)
        {
            _filmsProvider = filmProvider;
            _actorProvider = actorProvider;
            _userDbProvider = userDbProvider;
            _eventAggregator = eventAggregator;
            _localAggregator = new EventAggregator();
            _eventAggregator.Subscribe(this);
            _localAggregator.Subscribe(this);

            DisplayName = "";

            App.LanguageChanged += AppOnLanguageChanged;

            FilmCast = new ObservableCollection<MediaCast>();
            IsBusy = true;
        }
        #endregion
    }
}
