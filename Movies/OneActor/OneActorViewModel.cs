﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using Caliburn.Micro;
using Movies.Helpers;
using Movies.Helpers.EventAggregator;
using Services.Actors;
using Services.Films;

namespace Movies.OneActor
{
    sealed class OneActorViewModel : Screen, IHandle<Person>
    {
        #region Private Variables

        private readonly IActorProvider _actorProvider;
        private readonly IFilmsProvider _filmsProvider;
        private readonly IEventAggregator _eventAggregator;
        private readonly IEventAggregator _localAggregator;
        private Person _currentActor;

        #endregion

        #region Public Properties

        private PersonCredit _selectedPersonCredit;
        public PersonCredit SelectedPersonCredit
        {
            get { return _selectedPersonCredit; }
            set
            {
                if (_selectedPersonCredit != value)
                {
                    _selectedPersonCredit = null;
                    NotifyOfPropertyChange(() => SelectedPersonCredit);
                    PublishFilm(value);
                }
            }
        }

        private Person _actor;
        public Person Actor
        {
            get { return _actor; }
            set { SetValue(value); }
        }

        public ObservableCollection<PersonCredit> ActorCast { get; }
        public ObservableCollection<PersonCredit> Credits { get; set; }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        #endregion

        #region Private Methods

        private void AppOnLanguageChanged(object sender, EventArgs eventArgs)
        {
            _localAggregator.PublishOnUIThread(_currentActor);
            IsBusy = true;
        }

        private async void SetValue(Person man)
        {
            try
            {
                _actor = await _actorProvider.GetPersonById(man.Id, CultureInfo.CurrentUICulture);
                DisplayName = _actor.Name;
                ActorCast.Clear();
                var credits = await _actorProvider.GetPersonCredits(man.Id, CultureInfo.CurrentUICulture);
                Credits = new ObservableCollection<PersonCredit>(credits);
                NotifyOfPropertyChange(() => Credits);
                foreach (var credit in Credits.Take(7))
                {
                    if (!string.IsNullOrWhiteSpace(credit?.Title))
                        ActorCast.Add(credit);
                }
                NotifyOfPropertyChange(() => Actor);
                NotifyOfPropertyChange(() => ActorCast);
                IsBusy = false;
            }
            catch (AggregateException ex)
            {
                Logging.HandleNetworkError(new LogString(GetType().Name, "SetValue", ex,
                    "person id: " + man.Id));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (ServiceRequestException ex)
            {
                Logging.HandleRequestError(new LogString(GetType().Name, "SetValue", ex,
                    "person id: " + man.Id));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "SetValue", ex,
                    "person id: " + man.Id));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
        }

        private async void PublishFilm(PersonCredit credit)
        {
            try
            {
                var film = await _filmsProvider.GetFilmById(credit.Id, CultureInfo.CurrentUICulture);
                _eventAggregator.PublishOnUIThread(new DetailsMessage(film));
            }
            catch (AggregateException ex)
            {
                Logging.HandleNetworkError(new LogString(GetType().Name, "PublishFilm(PersonCredit)", ex,
                    "credit id: " + credit.Id));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (ServiceRequestException ex)
            {
                Logging.HandleRequestError(new LogString(GetType().Name, "PublishFilm(PersonCredit)", ex,
                    "credit id: " + credit.Id));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "PublishFilm(PersonCredit)", ex,
                    "credit id: " + credit.Id));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
        }

        #endregion

        #region Public Methods

        public void ShowAll()
        {
            _eventAggregator.PublishOnUIThread(new CreditsMessage(Actor));
        }

        #endregion

        #region IHandle Implementation

        public void Handle(Person actor)
        {
            _eventAggregator.Unsubscribe(this);
            _currentActor = actor;
            Actor = actor;
        }

        #endregion

        #region Costructors

        public OneActorViewModel(IActorProvider actorProvider, IFilmsProvider filmProvider,
            IEventAggregator eventAggregator)
        {
            _actorProvider = actorProvider;
            _filmsProvider = filmProvider;
            _eventAggregator = eventAggregator;
            _localAggregator = new EventAggregator();
            _eventAggregator.Subscribe(this);
            _localAggregator.Subscribe(this);

            DisplayName = "";

            App.LanguageChanged += AppOnLanguageChanged;

            ActorCast = new ObservableCollection<PersonCredit>();
            IsBusy = true;
        }

        #endregion
    }
}
