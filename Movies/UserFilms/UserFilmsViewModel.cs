﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.Mime;
using System.Net.TMDb;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using Movies.Helpers;
using Movies.Helpers.EventAggregator;
using Newtonsoft.Json;
using Services;
using Services.Series;
using Services.UserDB;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace Movies.UserFilms
{
    sealed class UserFilmsViewModel : Screen, IHandle<int>
    {
        #region Private Variables

        private readonly IUserDbProvider _provider;
        private readonly IEventAggregator _eventAggregator;
        private readonly IEventAggregator _localAggregator;
        private Func<int, CultureInfo, Task<PagedList<Resource>>> GetData;
        private Enums.UserFilmsCategoryEnum _currentCategory;

        #endregion

        #region Public Properties

        private Enums.View _viewProperty;
        public Enums.View ViewProperty
        {
            get { return _viewProperty; }
            set
            {
                if (_viewProperty != value)
                {
                    _viewProperty = value;
                    NotifyOfPropertyChange(() => ViewProperty);
                }
            }
        }

        private Resource _selectedItem;
        public Resource SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (_selectedItem != value)
                {
                    _eventAggregator.PublishOnUIThread(new DetailsMessage(value));
                    _selectedItem = null;
                    NotifyOfPropertyChange(() => SelectedItem);
                }
            }
        }

        private int _maxPage;
        public int MaxPage
        {
            set
            {
                if (value > 1000)
                    _maxPage = 1000;
                else if (value == 0)
                    _maxPage = 1;
                else
                    _maxPage = value;

                NotifyOfPropertyChange(() => CanMoveNext);
            }
            get { return _maxPage; }
        }

        private int _currentPage;
        public int CurrentPage
        {
            get { return _currentPage; }
            set
            {
                _currentPage = value;
                NotifyOfPropertyChange(() => CurrentPage);
                NotifyOfPropertyChange(() => CanMoveNext);
                NotifyOfPropertyChange(() => CanMovePrev);
            }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        public ObservableCollection<Resource> Items { get; }

        #endregion

        #region Private Methods

        private void AppOnLanguageChanged(object sender, EventArgs eventArgs)
        {
            IsBusy = true;
            if (_currentCategory != 0)
                _localAggregator.PublishOnUIThread((int)_currentCategory);
        }

        private async void RefreshItems()
        {
            try
            {
                if (GetData != null)
                {
                    Items.Clear();
                    var films = await GetData(CurrentPage, App.Language);
                    if (!films.Collection.Any())
                    {
                        IsBusy = false;
                        MessageBox.Show((string)Application.Current.Resources["LocNoUserFilms"]);
                        _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
                        _currentCategory = 0;
                    }
                    MaxPage = films.MaxPage;
                    foreach (var movie in films.Collection)
                        Items.Add(movie);
                    IsBusy = false;
                    NotifyOfPropertyChange(() => Items);
                }
            }
            catch (AggregateException ex)
            {
                Logging.HandleNetworkError(new LogString(GetType().Name, "RefreshItems", ex));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (ServiceRequestException ex)
            {
                Logging.HandleRequestError(new LogString(GetType().Name, "RefreshItems", ex));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "RefreshItems", ex));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
        }

        #endregion

        #region Paging Methods

        public bool CanMovePrev
        {
            get { return _currentPage > 1; }
        }

        public bool CanMoveNext
        {
            get { return _currentPage < MaxPage; }
        }

        public void MoveFirst()
        {
            if (CurrentPage == 1)
                return;
            CurrentPage = 1;
            RefreshItems();
        }

        public void MovePrev()
        {
            CurrentPage -= 1;
            RefreshItems();
        }

        public void MoveNext()
        {
            CurrentPage += 1;
            RefreshItems();
        }

        public void MoveLast()
        {
            if (CurrentPage == MaxPage)
                return;
            CurrentPage = MaxPage;
            RefreshItems();
        }

        public void ChangeView(int view)
        {
            ViewProperty = (Enums.View)view;
        }

        #endregion

        #region IHandle Implementation

        public void Handle(int category)
        {
            try
            {
                _eventAggregator.Unsubscribe(this);
                _currentCategory = (Enums.UserFilmsCategoryEnum) category;
                switch (_currentCategory)
                {
                    case Enums.UserFilmsCategoryEnum.Rated:
                        DisplayName = (string)Application.Current.Resources["LocRatedFilms"];
                        GetData = _provider.GetRatedMovies;
                        break;
                    case Enums.UserFilmsCategoryEnum.Commented:
                        DisplayName = (string)Application.Current.Resources["LocCommentedFilms"];
                        GetData = _provider.GetCommentedMovies;
                        break;
                    case Enums.UserFilmsCategoryEnum.Watchlist:
                        DisplayName = (string)Application.Current.Resources["LocWatchlist"];
                        GetData = _provider.GetWatchlist;
                        break;
                }
                RefreshItems();
            }
            catch (AggregateException ex)
            {
                Logging.HandleNetworkError(new LogString(GetType().Name, "Handle(int)", ex,
                    "category: " + (Enums.UserFilmsCategoryEnum)category));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (ServiceRequestException ex)
            {
                Logging.HandleRequestError(new LogString(GetType().Name, "Handle(int)", ex,
                    "category: " + (Enums.UserFilmsCategoryEnum)category));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "Handle(int)", ex,
                    "category: " + (Enums.UserFilmsCategoryEnum) category));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
        }

        #endregion

        #region Constructor

        public UserFilmsViewModel(IUserDbProvider provider, IEventAggregator eventAggregator)
        {
            _provider = provider;
            _eventAggregator = eventAggregator;
            _localAggregator = new EventAggregator();
            _eventAggregator.Subscribe(this);
            _localAggregator.Subscribe(this);

            IsBusy = true;
            Items = new ObservableCollection<Resource>();

            DisplayName = "";
            App.LanguageChanged += AppOnLanguageChanged;
            ViewProperty = Enums.View.GridView;
            CurrentPage = 1;
        }

        #endregion        
    }
}
