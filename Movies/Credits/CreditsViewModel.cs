﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using Movies.Helpers;
using Movies.Helpers.EventAggregator;
using Newtonsoft.Json;
using Services;
using Services.Actors;
using Services.Films;

namespace Movies.Credits
{
    sealed class CreditsViewModel : Screen, IHandle<Person>, IHandle<Movie>, IHandle<Show>
    {
        #region Private Variables

        private readonly IEventAggregator _eventAggregator;
        private readonly IActorProvider _actorProvider;
        private readonly IFilmsProvider _filmProvider;
        private readonly IEventAggregator _localEventAggregator;
        private object _currentItem;

        #endregion

        #region Public Properties

        public ObservableCollection<object> CastList { get; set; }
        public ObservableCollection<object> CrewList { get; set; }

        private string _castString;
        public string CastString
        {
            get { return _castString; }
            set
            {
                _castString = value;
                NotifyOfPropertyChange(() => CastString);
            }
        }

        private string _crewString;
        public string CrewString
        {
            get { return _crewString; }
            set
            {
                _crewString = value;
                NotifyOfPropertyChange(() => CrewString);
            }
        }

        private object _castSelectedItem;
        public object CastSelectedItem
        {
            get { return _castSelectedItem; }
            set
            {
                PublishItem(value);
                _castSelectedItem = null;
                NotifyOfPropertyChange(() => CastSelectedItem);
            }
        }

        private object _crewSelecteditem;
        public object CrewSelectedItem
        {
            get { return _crewSelecteditem; }
            set
            {
                PublishItem(value);
                _crewSelecteditem = null;
                NotifyOfPropertyChange(() => CrewSelectedItem);
            }
        }

        #endregion

        #region Private methods

        private async void PublishItem(object credit)
        {
            try
            {
                if (credit is PersonCredit)
                {
                    var id = ((PersonCredit) credit).Id;
                    var film = await _filmProvider.GetFilmById(id, CultureInfo.CurrentUICulture);
                    _eventAggregator.PublishOnUIThread(new DetailsMessage(film));
                }
                else if (credit is MediaCredit)
                {
                    var id = ((MediaCredit) credit).Id;
                    var actor = await _actorProvider.GetPersonById(id, CultureInfo.CurrentUICulture);
                    _eventAggregator.PublishOnUIThread(new DetailsMessage(actor));
                }
            }
            catch (AggregateException ex)
            {
                Logging.HandleNetworkError(new LogString(GetType().Name, "PublishItem", ex));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (ServiceRequestException ex)
            {
                Logging.HandleRequestError(new LogString(GetType().Name, "PublishItem", ex));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "PublishItem", ex));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
        }

        private void AppOnLanguageChanged(object sender, EventArgs eventArgs)
        {
            _localEventAggregator.PublishOnUIThread(_currentItem);
        }

        #endregion

        #region IHandle Implementation

        public async void Handle(Person man)
        {
            try
            {
                _eventAggregator.Unsubscribe(this);
                _currentItem = man;
                DisplayName = $"{Application.Current.Resources["LocCredits"]} {man.Name}";

                CastString = (string) Application.Current.Resources["LocCreditCast"];
                CrewString = (string) Application.Current.Resources["LocCreditCrew"];

                CastList.Clear();
                CrewList.Clear();
                var credits = await _actorProvider.GetPersonCredits(man.Id, CultureInfo.CurrentUICulture);
                foreach (var film in credits)
                    if (!string.IsNullOrWhiteSpace(film.Title))
                        if (film is PersonCast)
                            CastList.Add(film);
                        else
                            CrewList.Add(film);
            }
            catch (AggregateException ex)
            {
                Logging.HandleNetworkError(new LogString(GetType().Name, "Handle(Person)", ex,
                    "person id: " + man.Id));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (ServiceRequestException ex)
            {
                Logging.HandleRequestError(new LogString(GetType().Name, "Handle(Person)", ex,
                    "person id: " + man.Id));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "Handle(Person)", ex,
                    "person id: " + man.Id));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
        }

        public void Handle(Movie film)
        {
            try
            {
                _eventAggregator.Unsubscribe(this);
                _currentItem = film;
                DisplayName = $"{Application.Current.Resources["LocCommand"]} \"{film.Title}\"";

                CastString = (string)Application.Current.Resources["LocCast"];
                CrewString = (string)Application.Current.Resources["LocCrew"];

                CastList.Clear();
                CrewList.Clear();
                var cast = film.Credits.Cast;
                foreach (var man in cast)
                    CastList.Add(man);
                var crew = film.Credits.Crew;
                foreach (var man in crew)
                    CrewList.Add(man);
            }
            catch (AggregateException ex)
            {
                Logging.HandleNetworkError(new LogString(GetType().Name, "Handle(Movie)", ex,
                    "film id: " + film.Id));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (ServiceRequestException ex)
            {
                Logging.HandleRequestError(new LogString(GetType().Name, "Handle(Movie)", ex,
                    "film id: " + film.Id));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "Handle(Movie)", ex,
                    "film id: " + film.Id));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
        }

        public void Handle(Show show)
        {
            try
            {
                _eventAggregator.Unsubscribe(this);
                _currentItem = show;
                DisplayName = $"{Application.Current.Resources["LocCommand"]} \"{show.Name}\"";

                CastString = (string)Application.Current.Resources["LocCast"];
                CrewString = (string)Application.Current.Resources["LocCrew"];

                CastList.Clear();
                CrewList.Clear();
                var cast = show.Credits.Cast;
                foreach (var man in cast)
                    CastList.Add(man);
                var crew = show.Credits.Crew;
                foreach (var man in crew)
                    CrewList.Add(man);
            }
            catch (AggregateException ex)
            {
                Logging.HandleNetworkError(new LogString(GetType().Name, "Handle(Show)", ex,
                        "show id: " + show.Id));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (ServiceRequestException ex)
            {
                Logging.HandleRequestError(new LogString(GetType().Name, "Handle(Show)", ex,
                        "show id: " + show.Id));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "Handle(Show)", ex,
                    "show id: " + show.Id));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
        }

        #endregion

        #region Constructor

        public CreditsViewModel(IActorProvider actorProvider, IFilmsProvider filmProvider, IEventAggregator eventAggregator)
        {
            _actorProvider = actorProvider;
            _filmProvider = filmProvider;
            _eventAggregator = eventAggregator;
            _localEventAggregator = new EventAggregator();
            _eventAggregator.Subscribe(this);
            _localEventAggregator.Subscribe(this);

            DisplayName = "";

            App.LanguageChanged += AppOnLanguageChanged;

            CastList = new ObservableCollection<object>();
            CrewList = new ObservableCollection<object>();
        }

        #endregion
    }
}
