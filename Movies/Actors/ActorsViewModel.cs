﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using System.Windows;
using Caliburn.Micro;
using Services;
using Movies.Helpers;
using Movies.Helpers.EventAggregator;
using Newtonsoft.Json;
using Services.Actors;

namespace Movies.Actors
{
    sealed class ActorsViewModel : Conductor<object>, IHandle<string>
    {
        #region Private Variables

        private readonly IActorProvider _provider;
        private readonly IEventAggregator _eventAggregator;
        private string _query;

        #endregion

        #region Public Properties

        private Enums.View _viewProperty;
        public Enums.View ViewProperty
        {
            get { return _viewProperty; }
            set
            {
                if (_viewProperty != value)
                {
                    _viewProperty = value;
                    NotifyOfPropertyChange(() => ViewProperty);
                }
            }
        }

        private int _currentPage;
        public int CurrentPage
        {
            get { return _currentPage; }
            set
            {
                _currentPage = value;
                NotifyOfPropertyChange(() => CurrentPage);
                NotifyOfPropertyChange(() => CanMoveNext);
                NotifyOfPropertyChange(() => CanMovePrev);
            }
        }

        private Person _selectedActor;
        public Person SelectedActor
        {
            get { return _selectedActor; }
            set
            {
                if (_selectedActor != value)
                {
                    _eventAggregator.PublishOnUIThread(new DetailsMessage(value));
                    _selectedActor = null;
                    NotifyOfPropertyChange(() => SelectedActor);
                }
            }
        }

        private int _maxPage;
        public int MaxPage
        {
            set
            {
                _maxPage = value > 1000 ? 1000 : value;
                NotifyOfPropertyChange(() => CanMoveNext);
            }
            get { return _maxPage; }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        public ObservableCollection<Person> Actors { get; }

        #endregion

        #region Private Methods

        private void AppOnLanguageChanged(object sender, EventArgs eventArgs)
        {
            DisplayName = (string)Application.Current.Resources["LocCelebs"];
            IsBusy = true;
            RefreshItems();
        }

        private async void RefreshItems()
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(_query))
                {
                    Actors.Clear();
                    var actors = await _provider.FindActors(_query, CurrentPage, App.Language);
                    if (!actors.Results.Any())
                    {
                        _eventAggregator.PublishOnUIThread(new NoResults());
                        IsBusy = false;
                        return;
                    }
                    MaxPage = actors.PageCount;
                    foreach (var person in actors.Results)
                    {
                        Actors.Add(person);
                    }
                }
                IsBusy = false;
                NotifyOfPropertyChange(() => Actors);
            }
            catch (AggregateException ex)
            {
                Logging.HandleNetworkError(new LogString(GetType().Name, "RefreshItems", ex,
                    "query: " + _query));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (ServiceRequestException ex)
            {
                Logging.HandleRequestError(new LogString(GetType().Name, "RefreshItems", ex,
                    "query: " + _query));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "RefreshItems", ex,
                    "query: " + _query));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
        }

        #endregion

        #region Paging Methods

        public bool CanMovePrev
        {
            get { return _currentPage > 1; }
        }

        public bool CanMoveNext
        {
            get { return _currentPage < MaxPage; }
        }

        public void MoveFirst()
        {
            if (CurrentPage == 1)
                return;
            CurrentPage = 1;
            RefreshItems();
        }

        public void MovePrev()
        {
            CurrentPage -= 1;
            RefreshItems();
        }

        public void MoveNext()
        {
            CurrentPage += 1;
            RefreshItems();
        }

        public void MoveLast()
        {
            if (CurrentPage == MaxPage)
                return;
            CurrentPage = MaxPage;
            RefreshItems();
        }

        public void ChangeView(int view)
        {
            ViewProperty = (Enums.View)view;
        }

        #endregion

        #region IHandle Implementation

        public void Handle(string query)
        {
            _eventAggregator.Unsubscribe(this);
            _query = query;
            RefreshItems();
        }

        #endregion

        #region Constructor

        public ActorsViewModel(IActorProvider provider, IEventAggregator eventAggregator)
        {
            _provider = provider;
            _eventAggregator = eventAggregator;
            _eventAggregator.Subscribe(this);

            App.LanguageChanged += AppOnLanguageChanged;

            DisplayName = (string)Application.Current.Resources["LocCelebs"];

            ViewProperty = Enums.View.GridView;
            IsBusy = true;
            Actors = new ObservableCollection<Person>();
            CurrentPage = 1;
        }

        #endregion
    }
}