﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using Movies.Helpers;
using Movies.Helpers.EventAggregator;
using Newtonsoft.Json;
using Services;
using Services.Films;

namespace Movies.Films
{
    sealed class FilmsViewModel : Screen, IHandle<int>, IHandle<string>, IHandle<FilmQueryMessage>
    {
        #region Private Variables

        private readonly IFilmsProvider _provider;
        private readonly IEventAggregator _eventAggregator;
        private readonly IEventAggregator _localAggregator;
        private Func<int, CultureInfo, Task<System.Net.TMDb.Movies>> GetData;
        private string _title;
        private FilmQueryMessage _filmQuery;
        private Enums.FilmsCategoryEnum _currentCategory;

        #endregion

        #region Public Properties

        public bool Hello { get; set; }

        private Enums.View _viewProperty;
        public Enums.View ViewProperty
        {
            get { return _viewProperty; }
            set
            {
                if (_viewProperty != value)
                {
                    _viewProperty = value;
                    NotifyOfPropertyChange(() => ViewProperty);
                }
            }
        }

        private Movie _selectedFilm;
        public Movie SelectedFilm
        {
            get { return _selectedFilm; }
            set
            {
                if (_selectedFilm != value)
                {
                    _eventAggregator.PublishOnUIThread(new DetailsMessage(value));
                    _selectedFilm = null;
                    NotifyOfPropertyChange(() => SelectedFilm);
                }
            }
        }

        private int _maxPage;
        public int MaxPage
        {
            set
            {
                if (value > 1000)
                    _maxPage = 1000;
                else if (value == 0)
                    _maxPage = 1;
                else
                    _maxPage = value;

                NotifyOfPropertyChange(() => CanMoveNext);
            }
            get { return _maxPage; }
        }

        private int _currentPage;
        public int CurrentPage
        {
            get { return _currentPage; }
            set
            {
                _currentPage = value;
                NotifyOfPropertyChange(() => CurrentPage);
                NotifyOfPropertyChange(() => CanMoveNext);
                NotifyOfPropertyChange(() => CanMovePrev);
            }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        public ObservableCollection<Movie> Films { get; }

        #endregion

        #region Private Methods

        private void AppOnLanguageChanged(object sender, EventArgs eventArgs)
        {
            IsBusy = true;
            if (_currentCategory != 0)
            {
                _localAggregator.PublishOnUIThread((int)_currentCategory);
                return;
            }
            RefreshItems();
        }

        private async void RefreshItems()
        {
            try
            {
                if (GetData != null)
                {
                    Films.Clear();
                    var films = await GetData(CurrentPage, CultureInfo.CurrentUICulture);
                    MaxPage = films.PageCount;
                    foreach (var movie in films.Results)
                        Films.Add(movie);
                    IsBusy = false;
                    NotifyOfPropertyChange(() => Films);
                    return;
                }
            }
            catch (AggregateException ex)
            {
                Logging.HandleNetworkError(new LogString(GetType().Name, "RefreshItems", ex));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (ServiceRequestException ex)
            {
                Logging.HandleRequestError(new LogString(GetType().Name, "RefreshItems", ex));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "RefreshItems", ex));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }

            try
            {
                if (!string.IsNullOrEmpty(_title))
                {
                    Films.Clear();
                    var films = await _provider.FindMoviesByTitle(_title, CurrentPage, CultureInfo.CurrentUICulture);
                    if (!films.Results.Any())
                    {
                        IsBusy = false;
                        _eventAggregator.PublishOnUIThread(new NoResults());
                        return;
                    }
                    MaxPage = films.PageCount;
                    foreach (var movie in films.Results)
                        Films.Add(movie);
                    IsBusy = false;
                    NotifyOfPropertyChange(() => Films);
                    return;
                }
            }
            catch (AggregateException ex)
            {
                Logging.HandleNetworkError(new LogString(GetType().Name, "RefreshItems", ex, "title: " + _title));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (ServiceRequestException ex)
            {
                Logging.HandleRequestError(new LogString(GetType().Name, "RefreshItems", ex, "title: " + _title));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "RefreshItems", ex, "title: " + _title));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }

            try
            {
                if (_filmQuery != null)
                {
                    Films.Clear();
                    var films = await _provider.DiscoverMovies(_filmQuery.MinDate, _filmQuery.MaxDate,
                        _filmQuery.MinRating,
                        _filmQuery.Genres, _filmQuery.Actor, _filmQuery.Creator, CurrentPage, CultureInfo.CurrentUICulture);
                    if (!films.Results.Any())
                    {
                        IsBusy = false;
                        _eventAggregator.PublishOnUIThread(new NoResults());
                        return;
                    }
                    MaxPage = films.PageCount;
                    foreach (var movie in films.Results)
                        Films.Add(movie);
                    IsBusy = false;
                    NotifyOfPropertyChange(() => Films);
                }
            }
            catch (AggregateException ex)
            {
                Logging.HandleNetworkError(new LogString(GetType().Name, "RefreshItems", ex,
                        "filmQuery: " + JsonConvert.SerializeObject(_filmQuery)));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (ServiceRequestException ex)
            {
                Logging.HandleRequestError(new LogString(GetType().Name, "RefreshItems", ex,
                        "filmQuery: " + JsonConvert.SerializeObject(_filmQuery)));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "RefreshItems", ex,
                    "filmQuery: " + JsonConvert.SerializeObject(_filmQuery)));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
        }

        #endregion

        #region Paging Methods

        public bool CanMovePrev
        {
            get { return _currentPage > 1; }
        }

        public bool CanMoveNext
        {
            get { return _currentPage < MaxPage; }
        }

        public void MoveFirst()
        {
            if (CurrentPage == 1)
                return;
            CurrentPage = 1;
            RefreshItems();
        }

        public void MovePrev()
        {
            CurrentPage -= 1;
            RefreshItems();
        }

        public void MoveNext()
        {
            CurrentPage += 1;
            RefreshItems();
        }

        public void MoveLast()
        {
            if (CurrentPage == MaxPage)
                return;
            CurrentPage = MaxPage;
            RefreshItems();
        }

        public void ChangeView(int view)
        {
            ViewProperty = (Enums.View)view;
            Hello = true;
        }

        #endregion

        #region IHandle Implementation

        public void Handle(int category)
        {
            try
            {
                _eventAggregator.Unsubscribe(this);
                _currentCategory = (Enums.FilmsCategoryEnum)category;
                switch (_currentCategory)
                {
                    case Enums.FilmsCategoryEnum.Popular:
                        DisplayName = (string)Application.Current.Resources["LocPopularFilms"];
                        GetData = _provider.GetPopularMovies;
                        break;
                    case Enums.FilmsCategoryEnum.TopRated:
                        DisplayName = (string)Application.Current.Resources["LocTopFilms"];
                        GetData = _provider.GetTopRatedMovies;
                        break;
                    case Enums.FilmsCategoryEnum.NowPlaying:
                        DisplayName = (string)Application.Current.Resources["LocPlayingFilms"];
                        GetData = _provider.GetNowInTheatersMovies;
                        break;
                }
                RefreshItems();
            }
            catch (AggregateException ex)
            {
                Logging.HandleNetworkError(new LogString(GetType().Name, "Handle(int)", ex,
                        "category: " + (Enums.FilmsCategoryEnum)category));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (ServiceRequestException ex)
            {
                Logging.HandleRequestError(new LogString(GetType().Name, "Handle(int)", ex,
                        "category: " + (Enums.FilmsCategoryEnum)category));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
            catch (Exception ex)
            {
                Logging.HandleUnknownError(new LogString(GetType().Name, "Handle(int)", ex,
                    "category: " + (Enums.FilmsCategoryEnum) category));
                _eventAggregator.PublishOnUIThread(new ScreenMessage(this, true));
            }
        }

        public void Handle(string title)
        {
            _eventAggregator.Unsubscribe(this);
            _title = title;
            RefreshItems();
        }

        public void Handle(FilmQueryMessage query)
        {
            _eventAggregator.Unsubscribe(this);
            _filmQuery = query;
            RefreshItems();
        }

        #endregion

        #region Constructor

        public FilmsViewModel(IFilmsProvider provider, IEventAggregator eventAggregator)
        {
            _provider = provider;
            _eventAggregator = eventAggregator;
            _localAggregator = new EventAggregator();
            _eventAggregator.Subscribe(this);
            _localAggregator.Subscribe(this);

            DisplayName = "";

            App.LanguageChanged += AppOnLanguageChanged;

            ViewProperty = Enums.View.GridView;
            IsBusy = true;
            Films = new ObservableCollection<Movie>();

            CurrentPage = 1;
        }

        #endregion        
    }
}