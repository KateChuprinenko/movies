﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Services.Series
{
    public class SeriesProvider : ISeriesProvider
    {
        private readonly ServiceClient _client;

        public SeriesProvider()
        {
            _client = new ServiceClient("34eafca1437ceaf83c74c8153c7878b5");
        }

        public async Task<Shows> GetPopularShows(int page, CultureInfo language)
        {
            var series = await _client.Shows.GetPopularAsync(language.Name, page, CancellationToken.None);
            return series;
        }
        public async Task<Shows> GetTopRatedShows(int page, CultureInfo language)
        {
            var series = await _client.Shows.GetTopRatedAsync(language.Name, page, CancellationToken.None);
            return series;
        }
        public async Task<Show> GetShowById(int id, CultureInfo language)
        {
            var series = await _client.Shows.GetAsync(id, language.Name, true, CancellationToken.None);
            return series;
        }
        public async Task<Shows> FindShowsByTitle(string query, int page, CultureInfo language)
        {
            var series = await _client.Shows.SearchAsync(query, language.Name, null, true, page, CancellationToken.None);
            return series;
        }
        public Task<Shows> DiscoverShows(DateTime? min, DateTime? max, decimal? rating, string genres, int page, CultureInfo language)
        {
            var shows = _client.Shows.DiscoverAsync(language.Name, min, max, genres, rating, page, CancellationToken.None);
            return shows;
        }

        public async Task<Images> GetSeriesImages(int id)
        {
            var images = await _client.Shows.GetImagesAsync(id, null, null, null, CancellationToken.None);
            return images;
        }
    }
}
