﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading.Tasks;

namespace Services.Series
{
    public interface ISeriesProvider
    {
        Task<Shows> GetPopularShows(int page, CultureInfo language);
        Task<Shows> GetTopRatedShows(int page, CultureInfo language);
        Task<Shows> FindShowsByTitle(string query, int page, CultureInfo language);
        Task<Shows> DiscoverShows(DateTime? min, DateTime? max, decimal? rating, string genres, int page, CultureInfo language);

        Task<Show> GetShowById(int id, CultureInfo language);
        Task<Images> GetSeriesImages(int id);
    }
}
