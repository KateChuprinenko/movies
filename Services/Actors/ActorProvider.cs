﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Services.Actors
{
    public class ActorProvider : IActorProvider
    {
        private readonly ServiceClient _client;

        public ActorProvider()
        {
            _client = new ServiceClient("34eafca1437ceaf83c74c8153c7878b5");
        }

        public async Task<People> FindActors(string query, int page, CultureInfo language)
        {
            var actors = await _client.People.SearchAsync(query, true, true, page, language.Name, CancellationToken.None);
            return actors;
        }
        public async Task<Person> GetPersonById(int id, CultureInfo language)
        {
            var person = await _client.People.GetAsync(id, true, language.Name, CancellationToken.None);
            return person;
        }
        public async Task<IEnumerable<PersonCredit>> GetPersonCredits(int id, CultureInfo language)
        {
            var credits = await _client.People.GetCreditsAsync(id, language.Name, DataInfoType.Combined, CancellationToken.None);
            return credits;
        }
    }
}
