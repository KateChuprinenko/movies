﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading.Tasks;

namespace Services.Actors
{
    public interface IActorProvider
    {
        Task<People> FindActors(string query, int page, CultureInfo language);
        Task<Person> GetPersonById(int id, CultureInfo language);
        Task<IEnumerable<PersonCredit>> GetPersonCredits(int id, CultureInfo language);
    }
}
