﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class PagedList<T>
    {
        public IEnumerable<T> Collection { get; set; }
        public int MaxPage { get; set; }

        public PagedList(IEnumerable<T> collection, int maxPage)
        {
            Collection = new List<T>(collection);
            MaxPage = maxPage;
        }
    }
}
