﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.UserDB
{
    public class UserMovie
    {
        public int Rating { get; set; }
        public string Comment { get; set; }
        public bool Watchlist { get; set; }

        public UserMovie(int rating, string comment, bool watchlist)
        {
            Rating = rating;
            Comment = comment;
            Watchlist = watchlist;
        }
    }
}
