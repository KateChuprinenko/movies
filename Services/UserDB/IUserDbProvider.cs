﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading.Tasks;

namespace Services.UserDB
{
    public interface IUserDbProvider
    {
        UserMovie GetUserMovie(int id, int category);
        void SaveComment(int id, string comment, int category);
        void SaveRating(int id, int rating, int category);
        void SaveWatchlist(int id, bool watchlist, int category);
        Task<PagedList<Resource>> GetRatedMovies(int page, CultureInfo language);
        Task<PagedList<Resource>> GetCommentedMovies(int page, CultureInfo language);
        Task<PagedList<Resource>> GetWatchlist(int page, CultureInfo language);
    }
}
