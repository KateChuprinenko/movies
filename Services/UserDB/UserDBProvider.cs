﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using System.Threading;
using System.Threading.Tasks;
using DAL;

namespace Services.UserDB
{
    public class UserDbProvider : IUserDbProvider
    {
        private readonly ServiceClient _client;
        private readonly FilmsRepository _repository;
        private const int ItemsPerPage = 20;

        private int _getMaxPage(int count, int itemsPerPage)
        {
            var pages = count / itemsPerPage;
            if (count % itemsPerPage == 0)
                return pages;
            return ++pages;
        }

        public UserDbProvider()
        {
            _client = new ServiceClient("34eafca1437ceaf83c74c8153c7878b5");
            _repository = new FilmsRepository();
        }

        public UserMovie GetUserMovie(int id, int category)
        {
            var film = _repository.GetUserFilm(id, category);
            if (film != null)
                return new UserMovie(film.Rating, film.Comment, Convert.ToBoolean(film.Watchlist));
            return new UserMovie(0, null, false);
        }
        public void SaveComment(int id, string comment, int category)
        {
            _repository.SaveComment(id, comment, category);
        }
        public void SaveRating(int id, int rating, int category)
        {
            _repository.SaveRating(id, rating, category);
        }
        public void SaveWatchlist(int id, bool watchlist, int category)
        {
            _repository.SaveWatchlist(id, watchlist, category);
        }

        public async Task<PagedList<Resource>> GetRatedMovies(int page, CultureInfo language)
        {
            var result = new List<Resource>();
            var allItems = _repository.GetRatedFilms().ToList();
            var items = allItems
                .Skip(ItemsPerPage * (page - 1))
                .Take(ItemsPerPage);
            foreach (var item in items)
            {
                if (item.Category == 1)
                {
                    var film = await _client.Movies.GetAsync(item.ExternalId, language.Name, false, CancellationToken.None);
                    result.Add(film);
                }
                else
                {
                    var show = await _client.Shows.GetAsync(item.ExternalId, language.Name, false, CancellationToken.None);
                    result.Add(show);
                }
            }
            return new PagedList<Resource>(result, _getMaxPage(allItems.Count, ItemsPerPage));
        }
        public async Task<PagedList<Resource>> GetCommentedMovies(int page, CultureInfo language)
        {
            var result = new List<Resource>();
            var allItems = _repository.GetCommentedFilms().ToList();
            var items = allItems
                .Skip(ItemsPerPage * (page - 1))
                .Take(ItemsPerPage);
            foreach (var item in items)
            {
                if (item.Category == 1)
                {
                    var film = await _client.Movies.GetAsync(item.ExternalId, language.Name, false, CancellationToken.None);
                    result.Add(film);
                }
                else
                {
                    var show = await _client.Shows.GetAsync(item.ExternalId, language.Name, false, CancellationToken.None);
                    result.Add(show);
                }
            }
            return new PagedList<Resource>(result, _getMaxPage(allItems.Count, ItemsPerPage));
        }
        public async Task<PagedList<Resource>> GetWatchlist(int page, CultureInfo language)
        {
            var result = new List<Resource>();
            var allItems = _repository.GetWatchlist().ToList();
            var items = allItems
                .Skip(ItemsPerPage * (page - 1))
                .Take(ItemsPerPage);
            foreach (var item in items)
            {
                if (item.Category == 1)
                {
                    var film = await _client.Movies.GetAsync(item.ExternalId, language.Name, false, CancellationToken.None);
                    result.Add(film);
                }
                else
                {
                    var show = await _client.Shows.GetAsync(item.ExternalId, language.Name, false, CancellationToken.None);
                    result.Add(show);
                }
            }
            return new PagedList<Resource>(result, _getMaxPage(allItems.Count, ItemsPerPage));
        }
    }
}
