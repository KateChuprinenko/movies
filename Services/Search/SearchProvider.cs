﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Services.Search
{
    public class SearchProvider : ISearchProvider
    {
        private readonly ServiceClient _client;

        public SearchProvider()
        {
            _client = new ServiceClient("34eafca1437ceaf83c74c8153c7878b5");
        }

        public async Task<IEnumerable<Genre>> GetGenres(CultureInfo language)
        {
            var genres = await _client.Genres.GetAsync(DataInfoType.Combined, language.Name, CancellationToken.None);
            return genres;
        }
        public async Task<string> GetPeopleString(string name, CultureInfo language)
        {
            var people = await _client.People.SearchAsync(name, true, true, 1, language.Name, CancellationToken.None);
            var result = people.Results.FirstOrDefault()?.Id.ToString();
            return result;
        }
    }
}
