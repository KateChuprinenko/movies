﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Services.Search
{
    public interface ISearchProvider
    {
        Task<IEnumerable<Genre>> GetGenres(CultureInfo language);
        Task<string> GetPeopleString(string name, CultureInfo language);
    }
}
