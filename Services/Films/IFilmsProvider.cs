﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading.Tasks;

namespace Services.Films
{
    public interface IFilmsProvider
    {
        Task<Movies> GetPopularMovies(int page, CultureInfo language);
        Task<Movies> GetTopRatedMovies(int page, CultureInfo language);
        Task<Movies> GetNowInTheatersMovies(int page, CultureInfo language);
        Task<Movies> FindMoviesByTitle(string query, int page, CultureInfo language);
        Task<Movies> DiscoverMovies(DateTime? min, DateTime? max, decimal? rating, 
            string genres, string cast, string crew, int page, CultureInfo language);

        Task<Movie> GetFilmById(int id, CultureInfo language);
        Task<Images> GetMoviesImages(int id);
    }
}
