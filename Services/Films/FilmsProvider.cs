﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Services.Films
{
    public class FilmsProvider : IFilmsProvider
    {
        private readonly ServiceClient _client;

        public FilmsProvider()
        {
            _client = new ServiceClient("34eafca1437ceaf83c74c8153c7878b5");
        }

        public async Task<Movies> GetPopularMovies(int page, CultureInfo language)
        {
            var films = await _client.Movies.GetPopularAsync(language.Name, page, CancellationToken.None);
            return films;
        }
        public async Task<Movies> GetTopRatedMovies(int page, CultureInfo language)
        {
            var films = await _client.Movies.GetTopRatedAsync(language.Name, page, CancellationToken.None);
            return films;
        }
        public async Task<Movies> GetNowInTheatersMovies(int page, CultureInfo language)
        {
            var films = await _client.Movies.GetNowPlayingAsync(language.Name, page, CancellationToken.None);
            return films;
        }
        public async Task<Movies> FindMoviesByTitle(string query, int page, CultureInfo language)
        {
            var movies = await _client.Movies.SearchAsync(query, language.Name, true, null, true, page, CancellationToken.None);
            return movies;
        }
        public async Task<Movie> GetFilmById(int id, CultureInfo language)
        {
            var movie = await _client.Movies.GetAsync(id, language.Name, true, CancellationToken.None);
            return movie;
        }
        public async Task<Movies> DiscoverMovies(DateTime? min, DateTime? max, decimal? rating, string genres, string cast, string crew, int page, CultureInfo language)
        {
            var films = await _client.Movies.DiscoverAsync(language.Name, min, max, genres, cast, crew, rating, page, CancellationToken.None);
            return films;
        }
        public async Task<Images> GetMoviesImages(int id)
        {
            var images = await _client.Movies.GetImagesAsync(id, null, CancellationToken.None);
            return images;
        }
    }
}
