﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL
{
    using System.Data.Entity;

    public class FilmsContext : DbContext
    {
        public FilmsContext()
            : base("DefaultConnection")
        {
        }

        public virtual DbSet<UserFilm> UserFilms { get; set; }
    }
}
