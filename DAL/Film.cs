﻿namespace DAL
{
    using System;

    public class UserFilm
    {
        public int Id { get; set; }

        public int ExternalId { get; set; }
        public string Comment { get; set; }
        public int Rating { get; set; }
        public int Category { get; set; }
        public int Watchlist { get; set; }

        public UserFilm() { }

        public UserFilm(int id, int rating, bool watchlist, string comment, int category)
        {
            ExternalId = id;
            Comment = comment;
            Rating = rating;
            Category = category;
            Watchlist = Convert.ToInt32(watchlist);
        }
    }
}
