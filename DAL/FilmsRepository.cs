﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class FilmsRepository
    {
        private FilmsContext _context;

        public FilmsRepository()
        {
            _context = new FilmsContext();
            //_context.UserFilms.Load();
        }

        public UserFilm GetUserFilm(int id, int category)
        {
            var film = _context.UserFilms.FirstOrDefault(f => f.ExternalId == id && f.Category == category);
            return film;
        }

        public async void SaveComment(int id, string comment, int category)
        {
            var film = _context.UserFilms.FirstOrDefault(f => f.ExternalId == id && f.Category == category);
            if (film != null)
            {
                film.Comment = comment;
                _context.Entry(film).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            else
            {
                _context.UserFilms.Add(new UserFilm(id, 0, false, comment, category));
                await _context.SaveChangesAsync();
            }
        }
        public async void SaveRating(int id, int rating, int category)
        {
            var film = _context.UserFilms.FirstOrDefault(f => f.ExternalId == id && f.Category == category);
            if (film != null)
            {
                film.Rating = rating;
                _context.Entry(film).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            else
            {
                _context.UserFilms.Add(new UserFilm(id, rating, false, null, category));
                await _context.SaveChangesAsync();
            }
        }
        public async void SaveWatchlist(int id, bool watchlist, int category)
        {
            var film = _context.UserFilms.FirstOrDefault(f => f.ExternalId == id && f.Category == category);
            if (film != null)
            {
                film.Watchlist = Convert.ToInt32(watchlist);
                _context.Entry(film).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            else
            {
                _context.UserFilms.Add(new UserFilm(id, 0, watchlist, null, category));
                await _context.SaveChangesAsync();
            }
        }

        public IEnumerable<UserFilm> GetCommentedFilms()
        {
            return _context.UserFilms.Where(f => f.Comment != null);
        }
        public IEnumerable<UserFilm> GetRatedFilms()
        {
            return _context.UserFilms
                        .Where(f => f.Rating != 0)
                        .OrderByDescending(f => f.Rating);
        }
        public IEnumerable<UserFilm> GetWatchlist()
        {
            return _context.UserFilms.Where(f => f.Watchlist != 0);
        }
    }
}
